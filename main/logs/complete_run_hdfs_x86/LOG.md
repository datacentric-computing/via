Experiments performed at Ncloud using HDFS to distribute the data. 
Original scripts: /via/main/scripts/spark_with_traces/
Variables:
 - WORKERS: 1, 2 and 3.
 - CORES: 1, 2 and 4.
 - FILES: 1, 3 and 5.

The machines where bsc.l type with 8 VGPU and 16GB RAM. We performed three runs of all the experiments.

